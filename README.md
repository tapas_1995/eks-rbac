
# Setting up Kubernetes RBAC in Amazon EKS
## Define ClusterRole and ClusterRoleBindings  
### Once both the yaml files are configured, we can apply the changes using
```bash
kubectl apply -f cluster-role.yaml
kubectl apply -f cluster-role-binding.yaml

```
Note: This will grant read access to all resources across the cluster.

### Then, we need to map IAM users and roles to Kubernetes RBAC to control access to the Kubernetes API
#### Retrieve the existing aws-auth ConfigMap
```bash
kubectl get configmap/aws-auth -n kube-system -o yaml > aws-auth.yaml
```
 Edit the aws-auth.yaml file to add IAM user.

#### Apply the updated aws-auth ConfigMap
```bash
kubectl apply -f aws-auth.yaml
```





